package com.ibeyonde.net.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

import com.ibeyonde.net.utils.BadTokenException;
import com.ibeyonde.net.utils.Fifo;
import com.ibeyonde.net.utils.IpUtils;
import com.ibeyonde.net.utils.LiveMjpeg;
import com.ibeyonde.net.utils.NetUtils;
import com.ibeyonde.net.utils.StaticNatException;

public class LiveMp4 {
	private final static Logger LOG = Logger.getLogger(LiveMjpeg.class.getName());
	public Fifo _fifo = new Fifo(10);
	String _client_uuid = null;
	String _device_uuid = null; // static nat=31b9bbe7 / 20030(T-Hub Jio), 5128830a(mp4), 4d6711e1(mjpeg) (Home); restrictive nat=d97080b2/ (Thub Catalyst)
	String _username = null;
	int _port = 0;
	NetUtils _net = null;

	public LiveMp4(String cid, String uid, int port) {
		this._client_uuid = cid;
		this._username = uid;
		this._port = port;
	}
	
	public InetSocketAddress getPeerAddress(String did) throws IOException {
		this._device_uuid = did;
		_net = new NetUtils(_username, _client_uuid);
		DatagramPacket DpRcv = _net.register();
		DpRcv = _net.getPeerAddress(_device_uuid);
		return IpUtils.getAddress(DpRcv);
	}
	
	public String getToken(InetSocketAddress peer_address) throws StaticNatException, NoSuchAlgorithmException, IOException {
		String token = null;
		while( (token == null || token.length() == 0) && NetUtils.peer_receive_errors <= 4) {
			token = _net.getToken(_client_uuid, _username, _device_uuid, peer_address); 
			System.out.println("--------" + NetUtils.peer_receive_errors);
		}
		System.out.println("--------" + NetUtils.peer_receive_errors + " token=" + token);
		if ( NetUtils.peer_receive_errors >= 2 ) {
			throw new StaticNatException();
		}
		return token;
	}
	
	public byte[] getVideo(InetSocketAddress peer_address, String token, String q) throws IOException, BadTokenException {
		byte[] rcv_img = _net.getMp4Direct(token, _client_uuid, q, peer_address);
		return rcv_img;
	}

	// from jio home, jio works thub does not work=
	// From T-hub nothing works
	// From home: jio, home, work thub doesn;t

}
