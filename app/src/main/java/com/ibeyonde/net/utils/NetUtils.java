package com.ibeyonde.net.utils;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

public class NetUtils {
	private final static Logger LOG = Logger.getLogger(NetUtils.class.getName());


	public static final int _broker_port=5020;
	public static int peer_receive_errors=0;
	public static boolean peer_receive_initial=true;
	public static InetAddress _broker_address;
	private DatagramSocket _sock;
	public static InetAddress  _my_address;
	public static int  _my_port;
	public static String  _my_username;
	public static String  _my_uuid;
	private String TAG = "LiveViewActivity";
	public static final int min_udp_port = 20000;
	public static final int max_udp_port = 65535;

	private ByteBuffer _img_buf = ByteBuffer.allocate(20*1024*1024);

	public NetUtils(String username, String uuid) throws UnknownHostException, SocketException {
		if (uuid==null) throw new IllegalStateException("Invalid uuid");
		_my_username = username;
		_my_uuid = uuid;
		_my_port = getRandomUdpPort();
		_my_address = InetAddress.getLocalHost();
		_broker_address = InetAddress.getByName("broker.ibeyonde.com");
		//_broker_address = InetAddress.getLocalHost();
		_sock = new DatagramSocket();
		_sock.setReuseAddress(true);
		_sock.setSoTimeout(8000);
		_sock.setBroadcast(true);
	}

	public static int getRandomUdpPort() {
		return (int)((Math.random() * ((max_udp_port - min_udp_port) + 1)) + min_udp_port);
	}

	public DatagramPacket register() throws IOException {
		String cmd_str = new String("REGISTER:" + _my_uuid + ":");
		ByteBuffer cmd = ByteBuffer.allocate(cmd_str.length() + 6);
		cmd.put(cmd_str.getBytes());
		cmd.put(IpUtils.ipToBytes(_my_address.getHostAddress(), _my_port));
		sendCommandBroker(cmd);
		return recvCommandBroker();
	}

	public DatagramPacket getPeerAddress(String uuid) throws IOException {
		String cmd_str = new String("PADDR:" + uuid + ":");
		ByteBuffer cmd = ByteBuffer.allocate(cmd_str.length());
		cmd.put(cmd_str.getBytes());
		sendCommandBroker(cmd);
		return recvCommandBroker();
	}
	public void sendCommandBroker(String cmd_str) throws IOException {
		ByteBuffer cmd = ByteBuffer.allocate(cmd_str.length());
		cmd.put(cmd_str.getBytes());
		sendCommandBroker(cmd);
	}
	public void sendCommandBroker(ByteBuffer cmd) throws IOException {
		DatagramPacket DpSend =   new DatagramPacket(cmd.array(), cmd.capacity(), _broker_address, _broker_port);
		_sock.send(DpSend);
	}

	public DatagramPacket recvCommandBroker() throws IOException {
		byte[] buf = new byte[IpUtils.CMDCHUNK];
		DatagramPacket DpRcv = new DatagramPacket(buf, buf.length, _broker_address, _broker_port);
		_sock.receive(DpRcv);
		Log.i(TAG,"recvCommandBroker " + new String(DpRcv.getData()));
		return DpRcv;
	}

	public void sendCommandPeer(byte[] cmd, InetSocketAddress peer) throws IOException {
		DatagramPacket DpSend =   new DatagramPacket(cmd, cmd.length, peer.getAddress(), peer.getPort());
		_sock.send(DpSend);
	}

	public DatagramPacket recvCommandPeer(InetSocketAddress peer_address) throws IOException {
		DatagramPacket DpRcv = null;
		try {
			byte[] buf = new byte[IpUtils.CMDCHUNK];
			DpRcv = new DatagramPacket(buf, buf.length, peer_address.getAddress(), peer_address.getPort());
			_sock.receive(DpRcv);
			peer_receive_initial = false;
			peer_receive_errors = 0;
		}
		catch(java.net.SocketTimeoutException e) {
			if (peer_receive_initial)peer_receive_errors++;
		}
		return DpRcv;
	}

	public synchronized byte[] recvAllPeer(InetSocketAddress peer, String my_uuid, int size, byte[] bin_data) throws IOException {
		int remaining = size;
		_img_buf.clear();
		if (bin_data != null) {
			_img_buf.put(bin_data);
			remaining -= bin_data.length;
			Log.i(TAG,"Bin data " + bin_data.length);
		}
		byte[] buf = new byte[remaining];
		DatagramPacket DpRcv = new DatagramPacket(buf, buf.length, peer.getAddress(), peer.getPort());
		while (remaining > 0) {
			_sock.receive(DpRcv);
			_img_buf.put(buf, 0, DpRcv.getLength());
			remaining -= DpRcv.getLength();
			//Log.i(TAG,",r=" + remaining);
		}
		System.out.println("");
		_img_buf.flip();
		_img_buf.get(buf);
		return buf;
	}

	//username demo  device 31b9bbe7  ping = easJeQvk
	public String getToken(String client_uuid, String username, String device_uuid, InetSocketAddress peer_address) throws IOException, NoSuchAlgorithmException {
		String token = null;
		sendCommandPeer(("PING:" + client_uuid).getBytes(), peer_address);
		DatagramPacket DpRcv = recvCommandPeer(peer_address);
		System.out.println("rcvd from peer " + new String(IpUtils.getData(DpRcv)));

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(new String(username + device_uuid + IpUtils.getData(DpRcv)).getBytes());
		sendCommandPeer(("TOKEN:" + IpUtils.bytesToHex(md.digest())).getBytes(), peer_address);
		DpRcv = recvCommandPeer(peer_address);
		System.out.println("rcvd from peer " + new String(DpRcv.getData()));
		token =  new String(IpUtils.getData(DpRcv));
		System.out.println("TOKEN:" + token);
		return token;
	}

	public byte[] getImageDirect(String token, String client_uuid, String quality, InetSocketAddress peer_address) throws IOException, BadTokenException {
		byte[] rcv_img = null;
		try {
			sendCommandPeer(("DHINI:" + token).getBytes(), peer_address);
			System.out.println("DHINI: send to peer");
			DatagramPacket DpRcv = recvCommandPeer(peer_address);
			System.out.println("rcvd from peer " + new String(DpRcv.getData()));
			peer_receive_initial = false;
			peer_receive_errors = 0;
			String cmd_str = new String(DpRcv.getData());
			if (cmd_str.startsWith("SIZE")) {
				String cmd_size_str = cmd_str.substring("SIZE:".length());
				String[] cmd_size = cmd_size_str.split("\\.");
				int size = Integer.parseInt(cmd_size[1].trim());
				String cur_uuid = cmd_size[0];
				System.out.println("Size = " + size + " cur uuid=" + cur_uuid);
				rcv_img = recvAllPeer(peer_address, client_uuid, size, null);
				System.out.println("rcvd from peer " + rcv_img.length);
			}
			else if (cmd_str.startsWith("TOKEN")) {
				System.out.println("Bad Token");
				throw new BadTokenException();
			}
		}
		catch (SocketTimeoutException ex) {
			System.out.println("WARNING " + ex.getMessage());
			if (peer_receive_initial)peer_receive_errors++;
		}
		return rcv_img;
	}


	public byte[] getMp4Direct(String token, String client_uuid, String quality, InetSocketAddress peer_address) throws IOException, BadTokenException {
		byte[] rcv_img = null;
		try {
			sendCommandPeer(("DVG:" + token).getBytes(), peer_address);
			System.out.println("DVG: send to peer");
			DatagramPacket DpRcv = recvCommandPeer(peer_address);
			System.out.println("rcvd from peer " + new String(DpRcv.getData()));
			peer_receive_initial = false;
			peer_receive_errors = 0;
			String cmd_str = new String(DpRcv.getData(), 0, DpRcv.getLength());
			if (cmd_str.startsWith("SIZE")) {
				String size_cmd_str = cmd_str.substring("SIZE:".length());
				String[] cmd_size = size_cmd_str.split("\\.");
				byte[] bin_data = null;
				if (cmd_size.length == 4) {
					bin_data = cmd_size[3].getBytes();
					System.out.println("Bindata=" + bin_data + ", " +  bin_data.length);
				}
				int size = Integer.parseInt(cmd_size[1].trim());
				String cur_uuid = cmd_size[0];
				Log.i(TAG,"data parts = " + cmd_size.length + ":" + cur_uuid +  ", "  + cmd_size[1] + ", " + cmd_size[2] );
				if (size > 0) {
					rcv_img = recvAllPeer(peer_address, client_uuid, size, bin_data);
					Log.i(TAG,"rcvd from peer " + rcv_img.length);
				}
			}
			else if (cmd_str.startsWith("TOKEN")) {
				System.out.println("Bad Token");
				throw new BadTokenException();
			}
		}
		catch (SocketTimeoutException ex) {
			System.out.println("WARNING " + ex.getMessage());
			if (peer_receive_initial)peer_receive_errors++;
		}
		return rcv_img;
	}


	// ("192.168.100.17", 23456)
	// Long ip = 3232261137 int port = 23456
	// port short is unsigned short (16 bit, little endian byte order) java short
	// ip long is unsigned long (32 bit, big endian byte order) java int


}
