package com.ibeyonde.net.utils;

public class VQuality {
	public static final String [] quality = { "VA", "VB", "VC", "VD", "VE", "VF", "VG", "VH", "VI", "VJ", "VK", "VL", "VM", "VN", "VO", "VP" };
    

    public static String getNextQ(String q){
    		int i=0;
    		for (i=0; i < quality.length; i++) {
    			if (quality[i].equals(q)) break;
    		}
    		if (i<quality.length)
    			return quality[i++];
    		else 
    			return quality[i];
    }
    
    public static String getPrevQ(String q){
		int i=0;
		for (i=0; i < quality.length; i++) {
			if (quality[i].equals(q)) break;
		}
		if (i>0)
			return quality[i--];
		else 
			return quality[i];
    }
    
    public static String getQuality(Fifo fifo, String topQi){
        int rel = fifo.reliability();
        if (rel > 90){
            return getNextQ(topQi);
        }
        else if (rel > 30){
            return topQi;
        }
        else {
            return getPrevQ(topQi);
        }
    }
}
