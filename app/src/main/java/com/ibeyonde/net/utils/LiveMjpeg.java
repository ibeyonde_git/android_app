package com.ibeyonde.net.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.util.logging.Logger;
import java.security.NoSuchAlgorithmException;

public class LiveMjpeg {
	private final static Logger LOG = Logger.getLogger(LiveMjpeg.class.getName());
	public Fifo _fifo = new Fifo(10);
	String _client_uuid = null;
	String _device_uuid = null; // static nat=31b9bbe7(T-Hub Jio), 166f34ab (Home); restrictive nat=d97080b2/ (Thub Catalyst), 
	String _username = null;
	int _port = 0;
	NetUtils _net = null;

	public LiveMjpeg(String cid, String uid, int port) {
		this._client_uuid = cid;
		this._username = uid;
		this._port = port;
	}
	
	public InetSocketAddress getPeerAddress(String did) throws IOException {
		this._device_uuid = did;
		_net = new NetUtils(_username, _client_uuid);
		DatagramPacket DpRcv = _net.register();
		DpRcv = _net.getPeerAddress(_device_uuid);
		return IpUtils.getAddress(DpRcv);
	}
	
	public String getToken(InetSocketAddress peer_address) throws StaticNatException, NoSuchAlgorithmException, IOException {
		String token = null;
		while( (token == null || token.length() == 0) && NetUtils.peer_receive_errors <= 4) {
			token = _net.getToken(_client_uuid, _username, _device_uuid, peer_address); 
			System.out.println("--------" + NetUtils.peer_receive_errors);
		}
		System.out.println("--------" + NetUtils.peer_receive_errors + " token=" + token);
		if ( NetUtils.peer_receive_errors >= 2 ) {
			throw new StaticNatException();
		}
		return token;
	}
	
	public byte[] getImage(InetSocketAddress peer_address, String token, String q) throws IOException, BadTokenException {
		byte[] rcv_img = _net.getImageDirect(token, _client_uuid, q, peer_address);
		return rcv_img;
	}

	// from jio home, jio works thub does not work=
	// From T-hub nothing works
	// From home: jio, home, work thub doesn;t
	public static void main(String argv[]) throws IOException, NoSuchAlgorithmException, StaticNatException {
		LiveMjpeg mjpeg = new LiveMjpeg("zzzzzzz", "aprateek", 20013 );
		
		InetSocketAddress peer_address = mjpeg.getPeerAddress("166f34ab");
		String token = mjpeg.getToken(peer_address);
		
		
		for (int i = 0; i < 10; i++) {
			try {
					byte[] rcv_img = mjpeg.getImage(peer_address, token, "HINI");
					if (rcv_img != null) {
						OutputStream outputStream = new FileOutputStream("/Users/aprateek/Downloads/img" + i + ".jpg");
						outputStream.write(rcv_img);
						outputStream.close();
					}
			} catch (SocketTimeoutException ex) {
				LOG.warning("ERROR :" + ex.getMessage());
			} catch (BadTokenException e) {
				LOG.warning("ERROR : Bad token " +  e.getMessage());
				break;
			}
		}
		System.out.println("--------" + NetUtils.peer_receive_errors);
		//Fallback
	}
}
