package com.ibeyonde.net.utils;

public class Fifo {

	public int[] _queue = null;
	public int _size = 10;
	public int _index = 0;

	public Fifo(int size) {
		this._size = size;
		_queue = new int[size];

		for (int i = this._size / 2; i < this._size; i++) {
			this._queue[i] = 1;
		}
		for (int i = this._size / 2; i < this._size; i++) {
			this._queue[i] = 0;
		}
	}

	public void queue(int item) {
		for (int i = this._size - 1; i > 0; i--) {
			this._queue[i] = this._queue[i - 1];
		}
	}

	public int get(int i) {
		return this._queue[i];
	}

	public int reliability() {
		int rel = 0;
		int total = 0;
		for (int i = 0; i < this._size; i++) {
			total = total + 1;
			rel += this._queue[i];
		}
		int rel_per = (rel * 100) / total;
		// error_log("reliability: ".$rel_per);
		return rel_per;
	}
}
