package com.technorabit.ibeyonde;

import android.content.Intent;

import android.net.sip.SipAudioCall;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.dms.datalayerapi.util.CommonPoolExecutor;

import com.technorabit.ibeyonde.adaptor.TabViewPagerAdapter;

import com.technorabit.ibeyonde.fragment.TabFragment;

import com.technorabit.ibeyonde.model.NotificationModel;
import com.technorabit.ibeyonde.sip.IncomingCallReceiver;
import com.technorabit.ibeyonde.util.DBUtil;
import com.technorabit.ibeyonde.util.SharedUtil;

import java.util.List;

public class Dashboard extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public SipManager manager = null;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabViewPagerAdapter adapter;
    public IncomingCallReceiver callReceiver;
    public SipProfile me = null;
    public SipAudioCall call = null;

    private ImageView refresh;
    private NavigationView navigationView;
    private static final int UPDATE_SETTINGS_DIALOG = 3;
    TextView navigationText;
    DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        initGUI();
         navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        ((TextView) headerLayout.findViewById(R.id.username_side_nav)).setText(SharedUtil.get(this).getString("username"));
        navigationView.setNavigationItemSelectedListener(this);
        //

        Menu menuNav = navigationView.getMenu();

        navigationView.getMenu().getItem(2).setActionView(R.layout.menu_dot);

        navigationText = ( (LinearLayout)  navigationView.getMenu().getItem(2).getActionView()).findViewById(R.id.natification_count);

        setMenuCounter();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                           setMenuCounter();
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                setMenuCounter();

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });


    }








    @Override
    protected void onResume() {
        super.onResume();
        CommonPoolExecutor.get().clean();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    private void initGUI() {
        viewPager = findViewById(R.id.viewpager);
        setupViewPager();
        ((CheckBox) findViewById(R.id.checkBox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (adapter != null) {
                    for (int i = 0; i < adapter.getCount(); i++) {
                        ((TabFragment) adapter.getItem(i)).updateGrid(isChecked);
                    }
                }
            }
        });
        refresh = findViewById(R.id.refresh_view);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter != null) {
                    for (int i = 0; i < adapter.getCount(); i++) {
                        ((TabFragment) adapter.getItem(i)).refershViews();
                    }
                }
            }
        });
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


    }

    private void setupViewPager() {
        updateView();
    }

    private void updateView() {
        adapter = new TabViewPagerAdapter(getSupportFragmentManager());
        if (!(SharedUtil.get(this).hasKey("DefaultView")) || SharedUtil.get(this).getString("DefaultView").equalsIgnoreCase("Motion")) {
            adapter.addFragment(TabFragment.getInstance(TabFragment.Type.MOTION), "Motion");
            adapter.addFragment(TabFragment.getInstance(TabFragment.Type.LIVE), "Live");
        } else {
            adapter.addFragment(TabFragment.getInstance(TabFragment.Type.LIVE), "Live");
            adapter.addFragment(TabFragment.getInstance(TabFragment.Type.MOTION), "Motion");
        }
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_settings) {
             Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, 101);

        }else if (id == R.id.nav_logout) {
            SharedUtil.get(this).addToSet(SharedUtil.IS_LOGIN, false).commitSet();
            DBUtil.getInstance(this).deleteRow(NotificationModel.class,null);
            int  x =DBUtil.getInstance(this).getAllValuesFromTable(NotificationModel.class, "createdOn desc").size();

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_notifications) {
            //SharedUtil.get(this).addToSet(SharedUtil.IS_LOGIN, false).commitSet();
            Intent intent = new Intent(this, NotificationsActivity.class);
            startActivity(intent);
           // finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
        startActivity(new Intent(this, Dashboard.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adapter != null) {
            for (int i = 0; i < adapter.getCount(); i++) {
                ((TabFragment) adapter.getItem(i)).isDestroyed();
            }
        }
    }



    private void setMenuCounter( ) {
        List<NotificationModel> data = DBUtil.getInstance(this).getAllValuesFromTable("status=0",NotificationModel.class,"createdOn desc");
        navigationText.setText(""+data.size());
    }
}
