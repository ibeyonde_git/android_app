package com.technorabit.ibeyonde;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.technorabit.ibeyonde.util.SharedUtil;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Settings");
        }
        if (!SharedUtil.get(SettingsActivity.this).hasKey("DefaultView") || SharedUtil.get(SettingsActivity.this).getString("DefaultView").equalsIgnoreCase("Motion")) {
            ((RadioButton) findViewById(R.id.defaultMotion)).setChecked(true);
            ((RadioButton) findViewById(R.id.live)).setChecked(false);
        } else {
            ((RadioButton) findViewById(R.id.defaultMotion)).setChecked(false);
            ((RadioButton) findViewById(R.id.live)).setChecked(true);
        }
        ((RadioGroup) findViewById(R.id.radio_parrent)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.defaultMotion)
                    SharedUtil.get(SettingsActivity.this).store("DefaultView", "Motion");
                else
                    SharedUtil.get(SettingsActivity.this).store("DefaultView", "Live");
            }
        });

        if(SharedUtil.get(SettingsActivity.this).hasKey("passPref")){
            ((TextView)findViewById(R.id.sippassword)).setText( SharedUtil.get(SettingsActivity.this).getString("passPref"));
        }

        if(SharedUtil.get(SettingsActivity.this).hasKey("domainPref")){
            ((TextView)findViewById(R.id.sipdomain)).setText( SharedUtil.get(SettingsActivity.this).getString("domainPref"));
        }


        if(SharedUtil.get(SettingsActivity.this).hasKey("namePref")){
            ((TextView)findViewById(R.id.sipusername)).setText( SharedUtil.get(SettingsActivity.this).getString("namePref"));
        }


        findViewById(R.id.saveBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedUtil.get(SettingsActivity.this).store("passPref", ((TextView)findViewById(R.id.sippassword)).getText().toString());
                SharedUtil.get(SettingsActivity.this).store("domainPref",( (TextView)findViewById(R.id.sipdomain)).getText().toString());
                SharedUtil.get(SettingsActivity.this).store("namePref", ((TextView)findViewById(R.id.sipusername)).getText().toString());
                startActivity(new Intent(SettingsActivity.this, Dashboard.class));
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(RESULT_OK);
        finish();
        return super.onOptionsItemSelected(item);
    }
}
