package com.technorabit.ibeyonde;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.dms.datalayerapi.network.Http;
import com.dms.datalayerapi.util.GetUrlMaker;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;
import com.technorabit.ibeyonde.connection.HttpClientManager;
import com.technorabit.ibeyonde.constants.AppConstants;
import com.technorabit.ibeyonde.fragment.dailog.BaseFragmentDialog;
import com.technorabit.ibeyonde.model.LoginRes;
import com.technorabit.ibeyonde.util.SharedUtil;
import com.technorabit.ibeyonde.util.Util;

public class LoginActivity extends BaseActivity {

    private boolean isPermissionAvailable = false;
    private final int CALL_BACK_INT = 0x122;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initPermissions();
        initGUI();
    }

    private void initPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_SIP) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.USE_SIP,Manifest.permission.READ_PHONE_STATE}, CALL_BACK_INT);
            isPermissionAvailable = false;
            //noinspection UnnecessaryReturnStatement
            return;
        } else {
            isPermissionAvailable = true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CALL_BACK_INT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionAvailable = true;
                } else {
                }
            }
        }
    }


    private void initGUI() {
        findViewById(R.id.loginBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin(((EditText) findViewById(R.id.username)).getText().toString(), ((EditText) findViewById(R.id.password)).getText().toString());
            }
        });

        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
            }
        });
        findViewById(R.id.forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });


        ((EditText) findViewById(R.id.password)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    doLogin(((EditText) findViewById(R.id.username)).getText().toString(), ((EditText) findViewById(R.id.password)).getText().toString());
                    return true;
                }
                return false;
            }
        });

    }

    private void doLogin(final String username, final String password) {
        GetUrlMaker getUrlMaker = GetUrlMaker.getMaker();
        HttpClientManager client = HttpClientManager.get(LoginActivity.this);
        client.setUsername(username);
        client.setPassword(password);
        client.diskCacheEnable(false);
        final BaseFragmentDialog dialog = Util.showBaseLoading(LoginActivity.this);
        Log.i("Login", "Login Activity " + username + ", " + password);
        client.new NetworkTask<Void, LoginRes>(LoginRes.class, Http.GET) {
            @Override
            protected void onPostExecute(LoginRes loginData) {
                super.onPostExecute(loginData);
                dialog.dismiss();
                if (loginData != null) {
                    if (loginData.code == 200) {
                        SharedUtil.get(LoginActivity.this).addToSet("username", username).addToSet("password", password).addToSet(SharedUtil.IS_LOGIN, true).commitSet();
                        registerDevice();
                    }
                    Snackbar.make(getWindow().getDecorView().getRootView(), loginData.message, Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Bad  Credentials", Snackbar.LENGTH_LONG).show();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(AppConstants.LOGIN.replace(AppConstants.REPLACER, "")));
    }


    public void registerDevice(){
        String temp = "";
        final String deviceid = temp;
        FirebaseInstallations.getInstance().getToken(true).addOnCompleteListener(this, new OnCompleteListener<InstallationTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<InstallationTokenResult> task) {

                String deviceToken = task.getResult().getToken();
                GetUrlMaker getUrlMaker = GetUrlMaker.getMaker();
                HttpClientManager client = HttpClientManager.get(getBaseContext());
                client.setUsername(SharedUtil.get(getBaseContext()).getString("username"));
                client.setPassword(SharedUtil.get(getBaseContext()).getString("password"));
                client.diskCacheEnable(false);
                String url = AppConstants.PNS_REGISTRATION_URL.replace(AppConstants.REPLACER, "");

                url = url + "&username=" + SharedUtil.get(getBaseContext()).getString("username") + "&token=" + deviceToken + "&system=android&system_type=android&country=IN&language=en&phone_id="+deviceid;

                client.new NetworkTask<Void, String>(null, Http.GET) {
                    @Override
                    protected void onPostExecute(final String resp) {
                        super.onPostExecute(resp);
                        startActivity(new Intent(LoginActivity.this, Dashboard.class));
                        finish();
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(url));
            }

        });

    }
}
