package com.technorabit.ibeyonde;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;

import com.dms.datalayerapi.network.Http;
import com.dms.datalayerapi.util.GetUrlMaker;
import com.technorabit.ibeyonde.connection.HttpClientManager;
import com.technorabit.ibeyonde.constants.AppConstants;
import com.technorabit.ibeyonde.util.SharedUtil;

public class SplashActivity extends BaseActivity {

    private boolean isDestroy = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        satartThread();
    }

    private void satartThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startActivity();
            }
        }).start();
    }

    private void startActivity() {

        if (!isDestroy) {
            Bundle bundle = getIntent().getExtras();


            if (bundle != null && bundle.get("id")!=null) {


            }else {

                if (SharedUtil.get(this).getBoolean(SharedUtil.IS_LOGIN)) {
                    String temp = "";

                    final String deviceid = temp;
                    /**
                    FirebaseMessagingService.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
                        @Override
                        public void onSuccess(InstanceIdResult instanceIdResult) {
                            String deviceToken = instanceIdResult.getToken();
                            GetUrlMaker getUrlMaker = GetUrlMaker.getMaker();
                            HttpClientManager client = HttpClientManager.get(getBaseContext());
                            client.setUsername(SharedUtil.get(getBaseContext()).getString("username"));
                            client.setPassword(SharedUtil.get(getBaseContext()).getString("password"));
                            client.diskCacheEnable(false);
                            String url = AppConstants.PNS_REGISTRATION_URL.replace(AppConstants.REPLACER, "");
                            url = url + "&username=" + SharedUtil.get(getBaseContext()).getString("username") + "&token=" + deviceToken + "&system=android&system_type=android&country=IN&language=en&phone_id="+deviceid;

                            client.new NetworkTask<Void, String>(null, Http.GET) {
                                @Override
                                protected void onPostExecute(final String resp) {
                                    super.onPostExecute(resp);

                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(url));
                        }
                    });**/
                    startActivity(new Intent(this, Dashboard.class));
                } else
                    startActivity(new Intent(this, LoginActivity.class));
                finish();
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isDestroy = true;
    }
}
