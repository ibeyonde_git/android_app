package com.technorabit.ibeyonde.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pc on 06-07-2018.
 */

public class NotificationModel  {


    public NotificationModel() {

    }






    public String getDate() {
        return date;
    }

    public String getComment() {
        return comment;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }


    public String getType() {
        return type;
    }

    public String getUuid() {
        return uuid;
    }

    public String getId() {
        return id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }



    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }


    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }
    private String date;
    private String title;
    private String notificationID;

    private String type;
    private String uuid;
    private String id;
    private String comment;
    private String image;
    private String name;
    private int status;
    private long createdOn;



}
