package com.technorabit.ibeyonde.adaptor;

import android.view.View;

/**
 * Created by pc on 23-07-2018.
 */

public interface ItemClickListener {
    void onItemClick(View v, int pos);
}
