package com.technorabit.ibeyonde.adaptor;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.technorabit.ibeyonde.HistoryActivity;
import com.technorabit.ibeyonde.R;

import java.util.ArrayList;

/**
 * Created by raja on 07/05/18.
 */

public class BunchImageAdapter extends PagerAdapter {


    private ArrayList<String> images;
    private LayoutInflater inflater;
    private Context context;
    private String udid;


    public BunchImageAdapter(Context context, ArrayList<String> images,String udid) {
        this.context = context;
        this.images = images;
        this.udid = udid;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.bunch_image_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);

        final ProgressBar progressBar = (ProgressBar) imageLayout.findViewById(R.id.img_progress);

        RequestOptions requestOptions = new RequestOptions();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, HistoryActivity.class);
                intent.putExtra("udid", udid);

                context.startActivity(intent);
            }
        });
        requestOptions.placeholder(R.drawable.place);
        if(position>0) {
            progressBar.setVisibility(View.GONE);

            Glide.with(context).setDefaultRequestOptions(requestOptions).load(images.get(position)).into(imageView);
        } else{

            Glide.with(context)
                    .load(images.get(position))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);
        }
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}