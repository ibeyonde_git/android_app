package com.technorabit.ibeyonde.adaptor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.technorabit.ibeyonde.NotificationDetailActivity;
import com.technorabit.ibeyonde.R;
import com.technorabit.ibeyonde.model.NotificationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 11-07-2018.
 */

public class NotificationAdaptor  extends RecyclerView.Adapter<NotificationAdaptor.MyViewHolder> {

    private Context mContext;
    public List<NotificationModel> notificationModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtHeader;
        public TextView txtFooter;
        public TextView txtDummy;
        public View layout;

        public ImageView thumbnail;
        public TextView createdData;
        private ItemClickListener itemClickListener;
        public RelativeLayout viewBackground, viewForeground;


        public MyViewHolder(View view) {
            super(view);
            txtHeader = (TextView) view.findViewById(R.id.firstLine);
            txtFooter = (TextView) view.findViewById(R.id.secondLine);
            createdData = (TextView) view.findViewById(R.id.createdate);
            thumbnail = (ImageView) view.findViewById(R.id.icon);
            txtDummy = (TextView) view.findViewById(R.id.dummy);
            layout = view;
            //viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
            view.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v,getLayoutPosition());
        }
        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener=ic;

        }
    }


    public NotificationAdaptor(Context mContext, List<NotificationModel> notificationModelList) {
        this.mContext = mContext;
        this.notificationModelList = notificationModelList;
    }

    public void updateDataList(List<NotificationModel> notificationModelList){
        this.notificationModelList = null;

        this.notificationModelList = notificationModelList;
    }
    public void removeAll(){
        this.notificationModelList = new ArrayList<NotificationModel>();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_card, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final NotificationModel notificationModel = notificationModelList.get(position);

        holder.layout.setTag(R.string.n_id,notificationModel.getId());
        holder.txtHeader.setText(notificationModel.getTitle());
        holder.txtFooter.setText(notificationModel.getName());
        holder.createdData.setText(notificationModel.getDate());
        holder.txtDummy.setText(""+notificationModel.getStatus());

        Log.e("id",notificationModel.getId()+"--"+notificationModel.getStatus());


        if(notificationModel.getImage()!=null && notificationModel.getImage().trim().length()>0) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.placeholder);
            requestOptions.error(R.drawable.placeholder);
            Glide.with(mContext).applyDefaultRequestOptions(requestOptions).load(notificationModel.getImage()).into(holder.thumbnail);
        } else {


            Glide.with(mContext).load(R.drawable.mock).into(holder.thumbnail);
        }




        if(notificationModel != null && notificationModel.getStatus() == 0){
            holder.txtHeader.setTypeface(holder.txtHeader.getTypeface(), Typeface.BOLD_ITALIC);
            holder.txtFooter.setTypeface(holder.txtFooter.getTypeface(), Typeface.BOLD_ITALIC);
            holder.createdData.setTypeface(holder.createdData.getTypeface(), Typeface.BOLD_ITALIC);
        }else {
            holder.txtHeader.setTypeface(holder.txtHeader.getTypeface(), Typeface.NORMAL);
            holder.txtFooter.setTypeface(holder.txtFooter.getTypeface(), Typeface.NORMAL);
            holder.createdData.setTypeface(holder.createdData.getTypeface(), Typeface.NORMAL);
        }

        //WHEN ITEM IS CLICKED
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                String id =   (String)v.getTag(R.string.n_id);
              /*  holder.txtHeader.setTypeface(holder.txtHeader.getTypeface(), Typeface.NORMAL);
                holder.txtFooter.setTypeface(holder.txtFooter.getTypeface(), Typeface.NORMAL);
                holder.createdData.setTypeface(holder.createdData.getTypeface(), Typeface.NORMAL);*/
                //INTENT OBJ
                gotoDetails(id);

            }
        });
    }

    public  void gotoDetails(String id){
        Intent i = new Intent(mContext,NotificationDetailActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //ADD DATA TO OUR INTENT
        i.putExtra("notification_id",id);
        //START DETAIL ACTIVITY
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }

    public void removeItem(int position) {
        notificationModelList.remove(position);

        notifyItemRemoved(position);
    }

    public void restoreItem(NotificationModel item, int position) {
        notificationModelList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}