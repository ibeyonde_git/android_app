package com.technorabit.ibeyonde;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dms.datalayerapi.db.DBAction;
import com.technorabit.ibeyonde.adaptor.NotificationAdaptor;
import com.technorabit.ibeyonde.custom.SwipeControllerActions;
import com.technorabit.ibeyonde.model.NotificationModel;
import com.technorabit.ibeyonde.util.DBUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by pc on 13-07-2018.
 */

public class NotificationsActivity extends BaseActivity{
    private RecyclerView recyclerView;

    private static RecyclerView.LayoutManager layoutManager;
    private static NotificationAdaptor notificationAdaptor;
    private static List<NotificationModel> data;
    private FrameLayout coordinatorLayout;
    private TextView empty;
    SwipeController swipeController = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notifications);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Notifications");


        }
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        recyclerView = (RecyclerView) findViewById(R.id.my_notifications_recycler_view);
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = DBUtil.getInstance(this).getAllValuesFromTable(NotificationModel.class, "createdOn desc");


        notificationAdaptor = new NotificationAdaptor(getApplicationContext(), data);

        recyclerView.setAdapter(notificationAdaptor);

        empty = findViewById(R.id.list_empty);
        refershEmptyView();

        swipeController = new SwipeController(this,new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                deleteNotification(position);
            }
        });
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

    public void deleteNotification(final int position){

        final NotificationModel nm = notificationAdaptor.notificationModelList.get(position);


        DBUtil.getInstance(this).deleteRow(NotificationModel.class,"id = '"+nm.getId()+"'");

        //reloadList();
        notificationAdaptor.removeItem(position);
        notificationAdaptor.notifyItemRemoved(position);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, nm.getName() + " removed from list!", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DBUtil.getInstance(getApplicationContext()).insertOrUpdateTable(nm, DBAction.INSERT, null);
                reloadList();
                refershEmptyView();
            }
        });
        snackbar.setActionTextColor(Color.YELLOW);
        snackbar.show();
        refershEmptyView();
    }

    public void refershEmptyView() {
        if (notificationAdaptor.getItemCount() == 0) {
            empty.setGravity(Gravity.CENTER);
            empty.setVisibility(View.VISIBLE);

        } else {
            empty.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.notifications, menu);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int x = item.getItemId();
        if(item.getItemId()==R.id.action_refesh){
            reloadList();
        }else {
            Intent i = new Intent(this,Dashboard.class);
            startActivity(i);
            finish();

        }

        return true;
    }

    @Override
    public void onResume() {
        super.onResume(); //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the re
        //reloadList();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    public void reloadList(){
        data = DBUtil.getInstance(this).getAllValuesFromTable(NotificationModel.class, "createdOn desc");
        notificationAdaptor.updateDataList(data);
        for(int i = 0 ; i < data.size() ;i++){
            notificationAdaptor.notifyItemChanged(i);
        }
    }
}