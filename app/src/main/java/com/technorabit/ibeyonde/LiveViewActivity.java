package com.technorabit.ibeyonde;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.dms.datalayerapi.network.Http;
import com.dms.datalayerapi.util.GetUrlMaker;
import com.ibeyonde.net.utils.BadTokenException;
import com.ibeyonde.net.utils.LiveMp4;
import com.ibeyonde.net.utils.NetUtils;
import com.ibeyonde.net.utils.StaticNatException;
import com.technorabit.ibeyonde.connection.HttpClientManager;
import com.technorabit.ibeyonde.constants.AppConstants;
import com.technorabit.ibeyonde.util.SharedUtil;

import java.time.Instant;
import java.util.LinkedList;
import java.util.Queue;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import yjkim.mjpegviewer.MjpegView;

public class LiveViewActivity extends AppCompatActivity {
    private MjpegView video_view;
    //private VideoView videoView1;
    //private VideoView videoView2;
    private String URL;
    private int video_mode;
    private boolean sip_reg;
    private String udid;
    private Timer timerAsync;
    private TimerTask timerTaskAsync;
    private static final String TAG = "LiveViewActivity";
    private static final String TAG2 = "PLAYERActivity";
    private Timer timerIndexAsync;
    private TimerTask timerIndexTaskAsync;
    private long lastTimeStamp = 0;
    private int lastIndex = 0;
    private downloading mTask;
    public Player1prepare player1prepare;
    public Player2prepare player2prepare;
    private final int CALL_BACK_INT = 0x122;
    public String path;
    public Queue <String> q = new LinkedList<>();
    //private PlayerView playerView1;
    //private PlayerView playerView2;
    //public SimpleExoPlayer player1;
    //public SimpleExoPlayer player2;
    //public MediaSource videoSource;
    //ConcatenatingMediaSource mediaSource;

    String temp = "";
    Instant now;
    //DataSource.Factory dataSourceFactory ;

    String token = "";
    public boolean udpFlag = false;
    public boolean nextDownloading = true;
    int count = 0;
    int j =5;


    //boolean
    public boolean Player2waiting = false;
    public boolean Fallback = false;
    public boolean Player1waiting = false;


    /**
    private class PlayerEventListener extends Player.DefaultEventListener {

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            switch (playbackState) {
                case Player.STATE_IDLE:
                    Log.i("StateI","IDLE state");
                    playerView1.animate().alpha(0.7f);
                    // The player does not have any media to play yet.

                    break;
                case Player.STATE_BUFFERING:
                    Log.i("StateI","buffer state");// The player is buffering (loading the content)
                    playerView1.animate().alpha(0.7f);
                    break;
                case Player.STATE_READY:
                    Log.i("StateI","ready state");// The player is able to immediately play
                    playerView1.animate().alpha(1f);
                    break;
                case Player.STATE_ENDED:
                    Log.i("StateI","end state");// The player has finished playing the media
                    playerView1.animate().alpha(0.7f);
                    break;

            }
        }

    }
**/


    public class downloading extends AsyncTask {


        FileOutputStream outputStream;

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
/*
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(),
                    Util.getUserAgent(getApplicationContext(), "new_app"), null);
// This is the MediaSource representing the media to be played.


            if (q.size() > 0) {
                videoSource = new MediaSource[q.size()];
                int i = 0;
                while (q.size() > 0) {
                    videoSource[i] = new ExtractorMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(Uri.parse(path + "/" + q.remove()));

                    i = i + 1;

                }

            }
            Log.i(TAG, "dd");
            if (mediaSource == null) {
                mediaSource = new ConcatenatingMediaSource(videoSource);
            }

// Prepare the player with the source.

  */


        Fallback = true;


        }

        @Override
        protected Object doInBackground(Object[] objects) {

            try {

                startNewapp();

                Log.i(TAG, "downloading starts in background");


            } catch (Exception e) {
                Log.i(TAG, Log.getStackTraceString(e));
            }

            Fallback = true;
            return null;

        }

        public void startNewapp() throws IOException, NoSuchAlgorithmException, StaticNatException {

        try{



            LiveMp4 liveMp4 = new LiveMp4(temp, SharedUtil.get(getBaseContext()).getString("username"), NetUtils.getRandomUdpPort());
            Log.i(TAG, SharedUtil.get(getBaseContext()).getString("username"));
            Log.i(TAG, getIntent().getStringExtra("udid"));
            InetSocketAddress peer_address = liveMp4.getPeerAddress(getIntent().getStringExtra("udid"));

                token = liveMp4.getToken(peer_address);


                if (token.equals("badCredentials")) {
                    Log.w(TAG, "ERROR : Bad token, bailing out");
                    //System.exit(-1);
                }

                Log.i(TAG, "token for the transaction is"+token);
                for (int i = 0; i < 20; i++) {

                    //queue adding problem
                    // no stop after exiting app


                    Log.i(TAG, "inside loop");


                    try {
                        byte[] rcv_img = liveMp4.getVideo(peer_address, token,"Vp");
                        if (rcv_img != null) {
                            udpFlag = true;
                            Log.i(TAG,"flag is set to true");
                            Log.i(TAG, "starting ");
                            count = count + 1;


                            outputStream = openFileOutput("img" + i + ".mp4", Context.MODE_PRIVATE);
                            outputStream.write(rcv_img);
                            outputStream.close();
                            q.add("img" + i + ".mp4");


                        }

                        Thread.sleep(1000);
                    } catch (SocketTimeoutException ex) {
                        Log.i(TAG, "ERROR :" + ex.getMessage());
                    } catch (BadTokenException e) {
                        Log.i(TAG, "ERROR : Bad token " + e.getMessage());
                        break;
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                    }
                }
                Log.i(TAG, "--------" + NetUtils.peer_receive_errors);
                //Fallback


            } catch (StaticNatException ex) {
                Log.i(TAG, "ERROR :getToken " + ex.getMessage());
            }
            catch(SocketTimeoutException ex)
            {
                Log.i(TAG, "ERROR :SocketTimeout " + ex.getMessage());
            }

            Log.i(TAG, "total packet received" + count);

        }
    }



    public class Player1prepare extends AsyncTask {


        FileOutputStream outputStream;

        @Override
        protected void onPostExecute(Object o) {


           if(!Fallback) {
                super.onPostExecute(o);


/**
                Player1waiting = false;
               Log.i(TAG2, "player2 state" + Player.STATE_READY);
               try {
                   player1.prepare(mediaSource);
                   Log.i(TAG2, "player1 played");
               }
               catch(Exception e)
               {
                   Log.i(TAG2, "player1 stopped");
                   Fallback = true;
               }


                while (player2.getPlaybackState() == Player.STATE_READY && !Fallback);
                //playerView1.animate().alpha(0f).setDuration(1000);
                //hide player2
                //playerView1.setVisibility(View.VISIBLE);
                //playerView2.setVisibility(View.GONE);
               player2.setPlayWhenReady(false);

               playerView1.animate().alpha(1f).start();

               //playerView2.setVisibility(View.GONE);
                //playerView1.setVisibility(View.VISIBLE);


                player2prepare = new Player2prepare();
                player2prepare.executeOnExecutor(THREAD_POOL_EXECUTOR);
                playerView2.setVisibility(View.GONE);
                playerView1.setVisibility(View.VISIBLE);
                player1.setPlayWhenReady(true);


                //case may not be possible
            }


              else {


                playerView1.setVisibility(View.GONE);
                playerView2.setVisibility(View.GONE);
                //videoView1.setVisibility(View.VISIBLE);
                startBackgroundPerformForMP4Stream();
                startBackgroundPerformForMP4Index();


 **/

            }

        }

        @Override
        protected Object doInBackground(Object[] objects) {

            String value = "";

            Log.i(TAG2,"player 1 is getting video");

            while (!Fallback) {
                Log.i(TAG2,"player 1 is waiting for video");
                if (!Player2waiting)

                {
                    Player1waiting = true;
                    if (Fallback) {

                        break;
                    }

                    else
                    {

                        if(q.size()!=0)
                        {

                            value = q.remove();
                           /** videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                                    .createMediaSource(Uri.parse(path + "/" + value));
                            mediaSource = new ConcatenatingMediaSource();
                            mediaSource.addMediaSource(videoSource);
                            Log.i(TAG,"Player1 preparing with video"+value);**/
                            return null;

                        }
                    }


                }


            }


            return null;
        }

    }




    public class Player2prepare extends AsyncTask {


        FileOutputStream outputStream;

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if (!Fallback) {
                /**
                Player2waiting = false;
                Log.i(TAG2, "" + player1.getPlaybackState());
                Log.i(TAG2, "player1 state" + Player.STATE_READY);

                try {
                    player2.prepare(mediaSource);
                    Log.i(TAG2, "Player2 prepared");
                }
                catch(Exception e)
                {
                    Log.i(TAG2,"Player1 Stopped again");
                    Fallback = true;
                }




                while (player1.getPlaybackState() == Player.STATE_READY && !Fallback) ;

                player1.setPlayWhenReady(false);
                //hide player2

                player1prepare = new Player1prepare();
                player1prepare.executeOnExecutor(THREAD_POOL_EXECUTOR);


                playerView1.setVisibility(View.GONE);
                playerView1.setVisibility(View.VISIBLE);

                player2.setPlayWhenReady(true);


            }

            else {


                playerView1.setVisibility(View.GONE);
                playerView2.setVisibility(View.GONE);
                //videoView1.setVisibility(View.VISIBLE);
                startBackgroundPerformForMP4Stream();
                startBackgroundPerformForMP4Index();

**/
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {


            Log.i(TAG2,"player2 is geting video");
            String value = "";


            while (!Fallback) {
                Log.i(TAG2,"player2 is waiting for video");
                if (!Player1waiting)

                {
                    Player2waiting = true;
                    if (Fallback) {

                        break;
                    }

                    else
                    {

                        if(q.size()!=0)
                        {


                            value = q.remove();
                            Log.i(TAG,"Player2 is prepared with video"+value);
                            /**videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                                    .createMediaSource(Uri.parse(path + "/" + value));
                            mediaSource = new ConcatenatingMediaSource();
                            mediaSource.addMediaSource(videoSource);**/

                            return null;

                        }
                    }


                }


            }


            return null;
        }


    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_view);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.USE_SIP) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.USE_SIP, Manifest.permission.READ_PHONE_STATE}, CALL_BACK_INT);
            //noinspection UnnecessaryReturnStatement
            return;
        }

        mTask = new downloading();
        /**dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(),
                Util.getUserAgent(getApplicationContext(), "new_app"), null);**/



        Context context;
        context = getApplicationContext();

        path = context.getFilesDir().getPath();

        initData();



        }


    @Override
    protected void onStart() {
        super.onStart();

        Handler mainHandler = new Handler();
        //BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        /**TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        DefaultTrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

// 2. Create the player
         player1 =
                ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);
        player2 =
                ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);**/

        updateOnUI();

    }

    @Override
    protected void onStop() {

        //register shutdown function

        super.onStop();
        if (mTask != null){
            mTask.cancel(true);
            Log.i(TAG,"async cancelled");
        }
        if(timerAsync!=null) {
            timerAsync = null;
            timerTaskAsync.cancel();
        }
        if(timerIndexAsync!=null) {
            timerIndexAsync = null;
            timerIndexTaskAsync.cancel();
        }
        Log.i(TAG,"stop is called");
        Fallback = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initData() {
        setURL(getIntent().getStringExtra("hdurl"));
        setUDID(getIntent().getStringExtra("udid"));
        setVideoMode(getIntent().getIntExtra("video_mode",0));
}

    public void setURL(String URL) {
        this.URL = URL;
        Log.i(TAG,URL);
    }

    public void setUDID(String UDID) {
        this.udid = UDID;
    }
    public void setVideoMode(int video_mode ){
        this.video_mode = video_mode;
    }
    private void updateOnUI() {
       /** video_view = findViewById(R.id.mjpeg__hd_player);
        videoView1 = findViewById(R.id.mp4_video_view_1);
        videoView2 = findViewById(R.id.mp4_video_view_2);
        playerView1 = findViewById(R.id.player_view1);
        playerView2 = findViewById(R.id.player_view2);
        if(this.video_mode<1) {

            videoView1.setVisibility(View.GONE);
            videoView2.setVisibility(View.GONE);
            //video_view.Start(this.URL);
            //playerView1.setVisibility(View.GONE);
            //playerView2.setVisibility(View.GONE);

            mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            Log.i(TAG,"hello motto");
            player1prepare = new Player1prepare();
            player1prepare.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        }
        else {

            video_view.setVisibility(View.GONE);
            videoView1.setVisibility(View.GONE);
            videoView2.setVisibility(View.GONE);

            player1.setPlayWhenReady(false);
            player2.setPlayWhenReady(false);
            playerView1.setPlayer(player1);
            playerView2.setPlayer(player2);
            //Buffering feature




           // playerView2.setPlayer(player2);
           // player2.setPlayWhenReady(true);



           // String path = "file://android_asset/1.mp4";
         //   videoView1.setVideoPath(path).getPlayer().start()
            mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            Log.i(TAG,"Downlaind task is called");
            player1prepare = new Player1prepare();
           // player1.addListener(new PlayerEventListener());
            //player2.addListener(new PlayerEventListener());
            player1prepare.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);




           // startBackgroundPerformForMP4Stream();
           // startBackgroundPerformForMP4Index();
        }
**/
    }
    public String getUDUID() {
        return udid;
    }



    public void startBackgroundPerformForMP4Stream() {
        final Handler handler = new Handler();
        timerAsync = new Timer();
        timerTaskAsync = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            GetUrlMaker getUrlMaker = GetUrlMaker.getMaker();
                            HttpClientManager client = HttpClientManager.get(getBaseContext());
                            client.setUsername(SharedUtil.get(getBaseContext()).getString("username"));
                            client.setPassword(SharedUtil.get(getBaseContext()).getString("password"));
                            client.diskCacheEnable(false);
                            String url = AppConstants.MP4_STREAM_URL.replace(AppConstants.REPLACER, "");
                            url = url + "&uuid=" + getUDUID();
                            client.new NetworkTask<Void, String>(null, Http.GET) {
                                @Override
                                protected void onPostExecute(final String liveUrl) {
                                    super.onPostExecute(liveUrl);

                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(url));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timerAsync.schedule(timerTaskAsync, 0, 60*1000);
    }

    public void startBackgroundPerformForMP4Index() {
        final Handler handler = new Handler();
        timerIndexAsync = new Timer();
        timerIndexTaskAsync = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            GetUrlMaker getUrlMaker = GetUrlMaker.getMaker();
                            HttpClientManager client = HttpClientManager.get(getBaseContext());
                            client.setUsername(SharedUtil.get(getBaseContext()).getString("username"));
                            client.setPassword(SharedUtil.get(getBaseContext()).getString("password"));
                            client.diskCacheEnable(false);
                            String url = AppConstants.MP4_STREAM_INDEX.replace(AppConstants.REPLACER, "");
                            url = url + "&uuid=" + getUDUID();
                            client.new NetworkTask<Void, String>(null, Http.GET) {
                                @Override
                                protected void onPostExecute(final String indexes) {
                                    super.onPostExecute(indexes);
                                    if(indexes!=null && indexes.trim().length()>0){
                                        try {
                                            boolean indexChanged = false;
                                            JSONObject indexObject = new JSONObject(indexes);
                                            String list[] = indexObject.getString("index").split("\\:");
                                            String mp4url = "";
                                            if(lastTimeStamp == 0) {
                                                mp4url = "https://udp1.ibeyonde.com/live_cache/" + getUDUID() + "/vid" + list[0].split("\\-")[0] + ".mp4";
                                                indexChanged = true;
                                                lastIndex = Integer.parseInt(list[0].split("\\-")[0].trim());
                                                lastTimeStamp = Long.parseLong(list[0].split("\\-")[1].trim());
                                                //videoView1.setVideoPath(mp4url).getPlayer().start();
                                            }else{
                                                HashMap<Long, Integer> hmap = new HashMap<Long, Integer>();
                                                for(String l : list){
                                                    String parts[] = l.split("\\-");
                                                    hmap.put(Long.parseLong(parts[1]), Integer.parseInt(parts[0]));
                                                }
                                                Map<Long, Integer> map = new TreeMap<Long,Integer>(hmap);
                                                Set set2 = map.entrySet();
                                                Iterator iterator2 = set2.iterator();
                                                while(iterator2.hasNext()) {
                                                    Map.Entry me2 = (Map.Entry)iterator2.next();
                                                    if(Long.parseLong(String.valueOf(me2.getKey())) > lastTimeStamp ){
                                                        lastTimeStamp = Long.parseLong(String.valueOf(me2.getKey()));
                                                        mp4url = "https://udp1.ibeyonde.com/live_cache/" + getUDUID() + "/vid" + Integer.parseInt(String.valueOf(me2.getValue())) + ".mp4";
                                                        Log.i(TAG,mp4url);
                                                        //videoView1.setVideoPath(mp4url).getPlayer().start();
                                                        lastIndex = Integer.parseInt(String.valueOf(me2.getValue()));
                                                        indexChanged = true;
                                                        break;
                                                    }

                                                }
                                            }
                                            if(!indexChanged){
                                                mp4url = "https://udp1.ibeyonde.com/live_cache/" + getUDUID() + "/vid" + lastIndex + ".mp4";
                                                Log.i(TAG,mp4url);
                                                //videoView1.setVideoPath(mp4url).getPlayer().start();
                                            }

                                        }catch (Exception e){

                                        }
                                    }

                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(url))
                            ;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timerIndexAsync.schedule(timerIndexTaskAsync, 0, 8*1000);
    }

}