package com.technorabit.ibeyonde.util;

import android.content.Context;

import com.dms.datalayerapi.db.DBSupportUtil;
import com.dms.datalayerapi.db.core.TableDetails;

import java.util.ArrayList;

/**
 * Created by pc on 13-07-2018.
 */

public class DBUtil  extends DBSupportUtil{

    private static final int DB_VERSION_NUMBER = 1;
    public static final String DB_NAME = "db_file.writableDB";
    private static DBUtil instance = null;


    private DBUtil(Context context) {
        super(context);
    }

    @Override
    protected ArrayList<Class> getAllTables(ArrayList<Class> classes) {
        return null;
    }

    @Override
    protected ArrayList<TableDetails> getAllTableDetails(ArrayList<TableDetails> allTableDefinitions) {
        allTableDefinitions.add(TableDetails.getTableDetails(com.technorabit.ibeyonde.model.NotificationModel.class));
        return allTableDefinitions;
    }

    @Override
    protected String getDatabaseFileName() {
        return DB_NAME;
    }

    @Override
    public int getDatabaseVersion() {
        return DB_VERSION_NUMBER;
    }

    public synchronized static DBUtil getInstance(Context context) {
        if (instance == null) {
            instance = new DBUtil(context);
        }
        return instance;
    }
}
