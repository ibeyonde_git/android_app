package com.technorabit.ibeyonde;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.dms.datalayerapi.db.DBAction;
import com.technorabit.ibeyonde.model.NotificationModel;
import com.technorabit.ibeyonde.util.DBUtil;

import java.util.List;

/**
 * Created by pc on 23-07-2018.
 */

public class NotificationDetailActivity extends BaseActivity {
    ImageView img;
    TextView nameTxt;
    TextView txtComments;
    TextView txtCreatedOn;
    TextView txtUUID;
    TextView txtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);


        //RECEIVE OUR DATA
        Intent i = getIntent();
        final String id = i.getExtras().getString("notification_id");

        List<NotificationModel> notificationModelList =   DBUtil.getInstance(this).getAllValuesFromTable("id = '"+id+"'",NotificationModel.class,null);
        if(notificationModelList.isEmpty()){
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle("Not item found with id : "+id);
            }
        }else {
            NotificationModel notificationModel = notificationModelList.get(0);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle(notificationModel.getTitle());
            }
            //REFERENCE VIEWS FROM XML
            img = (ImageView) findViewById(R.id.notificationImage);
            nameTxt = (TextView) findViewById(R.id.natificationTitle);

            txtComments = (TextView) findViewById(R.id.comments);

            txtCreatedOn = (TextView) findViewById(R.id.createdon);
            txtUUID = (TextView) findViewById(R.id.uuid);
            txtName = (TextView) findViewById(R.id.devicename);

            //ASSIGN DATA TO THOSE VIEWS
            if (notificationModel.getImage() != null && notificationModel.getImage().trim().length() > 0) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.placeholder);
                requestOptions.error(R.drawable.placeholder);
                Glide.with(this).applyDefaultRequestOptions(requestOptions).load(notificationModel.getImage()).into(img);
            } else {
                Glide.with(this).load("http://www.ibeyonde.com/wp-content/uploads/2018/06/cloud_camera.png").into(img);
            }
            img.setOnTouchListener(new ImageMatrixTouchHandler(this));
            nameTxt.setText(notificationModel.getTitle());
            // posTxt.setText(notificationModel.getBody());
            txtComments.setText(notificationModel.getComment() == null ? "--" : notificationModel.getComment());

            txtCreatedOn.setText(notificationModel.getDate() == null ? "--" : notificationModel.getDate());
            txtUUID.setText(notificationModel.getUuid() == null ? "--" : notificationModel.getUuid());
            txtName.setText(notificationModel.getName() == null ? "--" : notificationModel.getName());

            notificationModel.setStatus(1);

            DBUtil.getInstance(this).insertOrUpdateTable(notificationModel, DBAction.UPDATE,"id='"+notificationModel.getId()+"'");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}