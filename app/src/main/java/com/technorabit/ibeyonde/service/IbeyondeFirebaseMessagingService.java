package com.technorabit.ibeyonde.service;


import com.dms.datalayerapi.db.DBAction;
import com.google.firebase.messaging.FirebaseMessagingService;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;


import com.google.firebase.messaging.RemoteMessage;
import com.technorabit.ibeyonde.NotificationsActivity;
import com.technorabit.ibeyonde.R;
import com.technorabit.ibeyonde.model.NotificationModel;
import com.technorabit.ibeyonde.util.DBUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 29-06-2018.
 */

public class IbeyondeFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "IbeyondeFMS";
    public static  int NOTIFICATION_ID = 200;
    public static String GROUP_KEY_WORK_NOTIFICATIONS = "com.technorabit.ibeyonde..WORK_NOTIFICATIONS";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        NotificationModel notificationModel = new NotificationModel();
        if (remoteMessage.getData().size() > 0) {

            Log.e(TAG, "Message data payload: " + remoteMessage.getData().get("type"));

            notificationModel.setType(remoteMessage.getData().get("type"));
            notificationModel.setImage(remoteMessage.getData().get("image"));
            notificationModel.setUuid(remoteMessage.getData().get("uuid"));
            notificationModel.setId(remoteMessage.getData().get("id"));
            notificationModel.setDate(remoteMessage.getData().get("created"));
            notificationModel.setName(remoteMessage.getData().get("name"));

            notificationModel.setTitle(remoteMessage.getData().get("title"));
            notificationModel.setCreatedOn(System.currentTimeMillis());
            notificationModel.setStatus(0);
            DBUtil.getInstance(getApplicationContext()).insertOrUpdateTable(notificationModel, DBAction.INSERT,null);

            generateNotification(notificationModel);


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

        }


    }


    private void generateNotification(NotificationModel notificationModel) {



        Intent resultIntent = new Intent(this, NotificationsActivity.class);
// Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
// Get the PendingIntent containing the entire back stack
        PendingIntent piResult =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);



        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "ibeyonde")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(notificationModel.getTitle())
                .setContentText(notificationModel.getName())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(piResult);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle(builder);
        inboxStyle.setBigContentTitle("Here Your Ibeyonde Alerts");




        List<NotificationModel> data = new ArrayList<NotificationModel>();

        data = DBUtil.getInstance(this).getAllValuesFromTable("status=0",NotificationModel.class,"createdOn desc");
        int counter = 0 ;
        for(NotificationModel notificationModel1 : data){
            inboxStyle.addLine(notificationModel1.getTitle());
            counter++;
            if(counter==3){
                break;
            }
        }
        if(data.size()>counter){
            inboxStyle.setSummaryText("+"+(data.size()-counter)+" alerts");
        }
        builder.setStyle(inboxStyle);
        Notification notification = inboxStyle.build();

       // NotificationManagerCompat
       // NotificationManager notificationManager =
              //  (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(NOTIFICATION_ID, notification);

    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN",s);
    }




}
